//
//  HomeViewController.swift
//  Wear it
//
//  Created by Rohit Arora on 17/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    var totalCombinationsArray: [Dictionary<String, AnyObject>] = []
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var noDataMessageLabel: UILabel!
    @IBOutlet weak var trouserImageView: UIImageView!
    @IBOutlet weak var shirtImageView: UIImageView!
    @IBOutlet weak var dressCombinationView: UIView!
    var currentIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        totalCombinationsArray = creatingCombinationSelected(getTotalItemsUser("Shirts"), trousers: getTotalItemsUser("Trousers"))
        setUIAccordingToData()
    }
    
    func setUIAccordingToData() {
        if totalCombinationsArray.count > 0 {
            noDataMessageLabel.hidden = true
            dressCombinationView.hidden = false
            buttonsView.hidden = false
            shareButton.hidden = false
            let dressCombinationDictionary = totalCombinationsArray[currentIndex]
            if let shirtUrl = dressCombinationDictionary["shirturl"] as? String {
                shirtImageView.image = UIImage(contentsOfFile: currentUserGalleryFolder("Shirts") + "/" + shirtUrl)
            }
            if let trouserUrl = dressCombinationDictionary["trouserUrl"] as? String {
                trouserImageView.image = UIImage(contentsOfFile: currentUserGalleryFolder("Trousers") + "/" + trouserUrl)
            }
        }else {
            buttonsView.hidden = true
            shareButton.hidden = true
            noDataMessageLabel.hidden = false
            dressCombinationView.hidden = true
        }
    }
    //MARK:- Share button action
    @IBAction func shareMyDressCombination(sender: UIButton) {
        let shareItems:Array = [dressCombinationView.screenShot()]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityTypePrint, UIActivityTypePostToFacebook, UIActivityTypeCopyToPasteboard, UIActivityTypeAddToReadingList, UIActivityTypePostToVimeo,UIActivityTypeMail,UIActivityTypeMessage]
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    //MARK:- Dislike button action
    @IBAction func dislikeCombinationButtonPressed(sender: UIButton) {
        showNextCombination()
    }
    //MARK:- Bookmark button action
    @IBAction func bookmarkThisCombinationMethod(sender: UIButton) {
        
        var userFacebookid = "", currentShirtUrl = "", currentTrouserUrl = ""
        let dressCombinationDictionary = totalCombinationsArray[currentIndex]
        if let shirtUrl = dressCombinationDictionary["shirturl"] as? String {
            currentShirtUrl=shirtUrl
        }
        if let trouserUrl = dressCombinationDictionary["trouserUrl"] as? String {
            currentTrouserUrl = trouserUrl
        }
        
        
        if let facebookData = NSUserDefaults.standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) as? [String:AnyObject] {
            if let facebookId = facebookData["userFacebookid"] as? String {
                userFacebookid = facebookId
            }
        }
        
        let finalUrl = currentShirtUrl + "@#@#" + currentTrouserUrl
        if(!bookmarkedCombnationsArray.containsObject(finalUrl)) {
            bookmarkedCombnationsArray.addObject(finalUrl)
            let dataPath = getDocumentsURL().stringByAppendingString("/\(userFacebookid)")
            print(dataPath)
            var tempArray = NSArray()
            tempArray = bookmarkedCombnationsArray
            let text = tempArray.componentsJoinedByString("\n")
            do {
                showNextCombination()
                try text.writeToFile("\(dataPath)/bookmarksFile.txt", atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    func showNextCombination() {
        totalCombinationsArray.removeAtIndex(currentIndex)
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.shirtImageView.alpha = 0
            self.trouserImageView.alpha = 0
            }) { (status) -> Void in
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    self.shirtImageView.alpha = 1
                    self.trouserImageView.alpha = 1
                })
                self.setUIAccordingToData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
