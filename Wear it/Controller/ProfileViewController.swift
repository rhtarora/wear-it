//
//  ProfileViewController.swift
//  Wear it
//
//  Created by Rohit Arora on 16/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userProfilePicture: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let facebookData = NSUserDefaults.standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) as? [String:AnyObject] {
            if let emailValue = facebookData["userEmailId"] as? String {
                userEmailLabel.text = emailValue
            }else {
                userEmailLabel.text = ""
                emailTitleLabel.text = ""
            }
            if let nameValue = facebookData["userName"] as? String {
                userNameLabel.text = nameValue
            }
            if let imageUrl = facebookData["userImageUrl"] as? String {
                userProfilePicture.setImageWithUrl(NSURL(string: imageUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!, placeHolderImage: UIImage(named: "profilePlaceholder"))
           }
        }
    }
    
    // MARK: - Logout button
    @IBAction func logoutButtonPressed(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { action in
            removePersistedDataOnLogout()
            let tc:UITabBarController = self.navigationController!.parentViewController as! UITabBarController
            tc.navigationController?.popToRootViewControllerAnimated(true)
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
