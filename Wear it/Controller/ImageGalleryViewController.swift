//
//  ImageGalleryViewController.swift
//  Wear it
//
//  Created by Rohit Arora on 17/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import UIKit

class ImageGalleryViewController: UIViewController {

    @IBOutlet weak var trouserCountLabel: UILabel!
    @IBOutlet weak var shirtCountLabel: UILabel!
    @IBOutlet weak var trousersDetailView: UIView!
    @IBOutlet weak var shirtDetailsView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        shirtDetailsView.layer.borderColor = appMainColor.CGColor
        trousersDetailView.layer.borderColor = appMainColor.CGColor
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        var itemCount = getTotalItemsUser("Shirts").count
        shirtCountLabel.text = "You have \(itemCount) shirts in your gallery."
        itemCount = getTotalItemsUser("Trousers").count
        trouserCountLabel.text = "You have \(itemCount) trousers in your gallery."
    }
    @IBAction func viewTapped(sender: UITapGestureRecognizer) {
        let ImageCollectionScreen = self.storyboard?.instantiateViewControllerWithIdentifier("ImageCollectionViewController") as! ImageCollectionViewController
        if sender.view?.tag == 1 {
           ImageCollectionScreen.itemType = "Shirts"
        }else{
           ImageCollectionScreen.itemType = "Trousers"
        }
        self.navigationController?.pushViewController(ImageCollectionScreen, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
