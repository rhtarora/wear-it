//
//  ImageCollectionViewController.swift
//  Wear it
//
//  Created by Rohit Arora on 17/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import UIKit

class ImageCollectionViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    @IBOutlet weak var imageGalleryCollectionView: UICollectionView!
    var itemType = ""
    @IBOutlet weak var headerTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTextLabel.text = itemType
        // Do any additional setup after loading the view.
    }
    
    @IBAction func popBackToPreviousScreen(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addNewImageButtonPressed(sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let GalleryAction: UIAlertAction = UIAlertAction(title: "Gallery", style: .Default) { action -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            self.openCameraOrGallery(.PhotoLibrary)
        }
        
        let CameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .Default ) { action -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            self.openCameraOrGallery(.Camera)
        }
        let CancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel ) { action -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(GalleryAction)
        alert.addAction(CameraAction)
        alert.addAction(CancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    //MARK: - Open camera or album to pick images
    func openCameraOrGallery(source:UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = source
        if source ==  UIImagePickerControllerSourceType.Camera {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
                presentViewController(imagePicker, animated: true, completion: nil)
            } else{
                Alert(self,alertMessage: "Camera not available!")
            }
        }else {
            presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - Image picker delegates
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let settingImageName = "\(NSDate())"
            var userFacebookid = ""
            if let facebookData = NSUserDefaults.standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) as? [String:AnyObject] {
                if let facebookId = facebookData["userFacebookid"] as? String {
                    userFacebookid = facebookId
                }
            }
            
            let imagePath = getDocumentsURL().stringByAppendingString("/\(userFacebookid)/\(itemType)/\(settingImageName).jpeg")
            print(imagePath)
            let jpegData = UIImageJPEGRepresentation(pickedImage, 0.5)
            jpegData!.writeToFile(imagePath, atomically: true)
            imageGalleryCollectionView.reloadData()
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK: - Collection view delegates
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getTotalItemsUser(itemType).count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCellWithReuseIdentifier("imageGalleryCellIdentifier", forIndexPath: indexPath) as! imageGalleryCollectionViewCell
        var imagePath : String!
        let imageNames = getTotalItemsUser(itemType)
        imagePath = imageNames[indexPath.row]
        imagePath = currentUserGalleryFolder(itemType) + "/" + imagePath
        NSLog("imagePath%@", imagePath)
        itemCell.itemImage.image = UIImage(contentsOfFile: imagePath)
        itemCell.backgroundColor = UIColor.clearColor()
        return itemCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

class imageGalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var itemImage : UIImageView!
}
