//
//  BookmarksViewController.swift
//  Wear it
//
//  Created by Rohit Arora on 17/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import UIKit

class BookmarksViewController: UIViewController {

    @IBOutlet weak var noDataMessageLabel: UILabel!
    @IBOutlet weak var bookmarkedCombinationsTableView: UITableView!
    var bookMarksArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        let textFileString = readTextFileFromDocumentDirectory("bookmarksFile.txt")
        if textFileString == "(\n)" {
            noDataMessageLabel.hidden = false
        }else{
            noDataMessageLabel.hidden = true
            bookMarksArray = textFileString.componentsSeparatedByString("\n")
            print(bookMarksArray)
            bookmarkedCombinationsTableView.reloadData()
        }
    }
    
    //MARK: - Table view delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookMarksArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let bookMarkCell = tableView.dequeueReusableCellWithIdentifier("bookmarkCellIdentifier", forIndexPath: indexPath) as! BookmarkTableViewCell
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            // do some task
            let combinedUrl = self.bookMarksArray[indexPath.row] 
            let arrayOfUrls = combinedUrl.componentsSeparatedByString("@#@#") as NSArray
            let shirtImage = UIImage(contentsOfFile: currentUserGalleryFolder("Shirts") + "/" + (arrayOfUrls.objectAtIndex(0) as! String) )
            let trouserImage = UIImage(contentsOfFile:currentUserGalleryFolder("Trousers") + "/" + (arrayOfUrls.objectAtIndex(1) as! String))
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                bookMarkCell.shirtImageView.image = shirtImage
                bookMarkCell.trouserImageView.image = trouserImage
            }
        }
        
        return bookMarkCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class BookmarkTableViewCell: UITableViewCell {
    @IBOutlet weak var shirtImageView: UIImageView!
    @IBOutlet weak var trouserImageView: UIImageView!
}
