//
//  ViewController.swift
//  Wear it
//
//  Created by Rohit Arora on 15/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var facebookLoginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(NSUserDefaults .standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) != nil){
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let tabBarController: UITabBarController = storyboard.instantiateViewControllerWithIdentifier("TabBarViewController") as! TabBarViewController
            tabBarController.selectedIndex = 1
            self.navigationController?.setViewControllers([tabBarController], animated: true)
        }
        
        facebookLoginButton.setBackgroundColor(facebookButtonNormalColor, forUIControlState: .Normal)
        facebookLoginButton.setBackgroundColor(facebookButtonPressedColor, forUIControlState: .Highlighted)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // Button to call facebook login API from network help class.
    @IBAction func facebookLoginButtonPressed(sender: UIButton) {
        if !Reachability.isConnectedToNetwork() {
            Alert(self, alertMessage: ErrorInternetConnection)
        }
        else {
            activityIndicator.startAnimating()
            facebookLoginButton.setTitle("Please wait...", forState: .Normal)
            NetworkHelper.facebookLoginOperation(self, completion: { (status) -> Void in
                if status == 1 {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let tabBarController: UITabBarController = storyboard.instantiateViewControllerWithIdentifier("TabBarViewController") as! TabBarViewController
                    
                   let totalCombinationsArray = creatingCombinationSelected(getTotalItemsUser("Shirts"), trousers: getTotalItemsUser("Trousers"))
                    
                    if totalCombinationsArray.count>0 {
                    tabBarController.selectedIndex = 1
                    }else {
                        tabBarController.selectedIndex = 0
                    }
                    self.navigationController?.pushViewController(tabBarController, animated: true)
                }
                self.activityIndicator.stopAnimating()
                self.facebookLoginButton.setTitle("Login with Facebook", forState: .Normal)
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

