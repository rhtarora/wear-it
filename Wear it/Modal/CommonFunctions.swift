//
//  CommonFunctions.swift

import UIKit
import Foundation
import SystemConfiguration

public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

var bookmarkedCombnationsArray = NSMutableArray()

//MARK: - Alert view
func Alert(ViewController : UIViewController , alertMessage : String) {
    
    let alertController = UIAlertController(title: "", message: alertMessage, preferredStyle: .Alert)
    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (action) in
        return
    }
    alertController.addAction(cancelAction)
    ViewController.presentViewController(alertController, animated: true) {
    }
}

//MARK: - Remove files with given path
func deleteFileAtPath(yourPath:String){
    do {
        try  NSFileManager.defaultManager().removeItemAtPath(yourPath)
    } catch let error as NSError {
        print(error.localizedDescription)
    }
}

//MARK: - Get user's items
func getTotalItemsUser(itemType:String) -> [String?] {
    var imageArray : [String?]
    imageArray = []
    var documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    if let facebookData = NSUserDefaults.standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) as? [String:AnyObject] {
        if let facebookId = facebookData["userFacebookid"] as? String {
            documentsUrl = documentsUrl.URLByAppendingPathComponent(facebookId)
        }
    }
    documentsUrl = documentsUrl.URLByAppendingPathComponent(itemType)
    do {
        let directoryUrls = try  NSFileManager.defaultManager().contentsOfDirectoryAtURL(documentsUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions())
        let jpegFiles = directoryUrls.filter(){ $0.pathExtension == "jpeg" }.map{ $0.lastPathComponent }
        return jpegFiles
        
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    return imageArray
}

//MARK: - Get user items folder path
func currentUserGalleryFolder(itemType:String) -> String {
    var facebookID = ""
    if let facebookData = NSUserDefaults.standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) as? [String:AnyObject] {
        if let facebookId = facebookData["userFacebookid"] as? String {
            facebookID = facebookId
        }
    }
    let path = getDocumentsURL().stringByAppendingString("/\(facebookID)/\(itemType)")
    return path
}

//MARK: - Combination dictionary

func creatingCombinationSelected(shirts:[String?],trousers:[String?]) -> [Dictionary<String,AnyObject>]
{
    var totalCombinationsArray: [Dictionary<String, AnyObject>] = []
    for i in 0..<shirts.count {
        for j in 0..<trousers.count {
            var combinationDictionary =  Dictionary<String,AnyObject>()
            combinationDictionary["shirturl"] = shirts[i]
            combinationDictionary[ "trouserUrl"] = trousers[j]
            combinationDictionary["isBookmarked"] = "0"
            totalCombinationsArray.append(combinationDictionary)
        }
    }
    return totalCombinationsArray
}

//MARK: - Create folders to store images
func createOrFetchUserInfoDatabaseInDirectory(UserFacebookId:String){
    
    let dataPath = getDocumentsURL().stringByAppendingString("/\(UserFacebookId)")
    
    if (!NSFileManager.defaultManager().fileExistsAtPath(dataPath)) {
        do {
            try NSFileManager.defaultManager().createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
            createFolderAtThisPath(dataPath, folderName: "/Shirts")
            createFolderAtThisPath(dataPath, folderName: "/Trousers")
            let documentUrl = getDocumentsURL()
            var dataPath = "\(documentUrl)".stringByAppendingString("/\(UserFacebookId)")
            dataPath = dataPath.stringByAppendingString("/bookmarksFile.txt")
            let text = "\(bookmarkedCombnationsArray)"
            do {
                try text.writeToFile(dataPath, atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch let error as NSError {
                print(error.localizedDescription);
            }
        } catch let error as NSError {
            print(error.localizedDescription);
        }
    }
}

//MARK: - Get document directory path
func getDocumentsURL() -> AnyObject {
    
    let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
    let documentsDirectory: AnyObject = paths[0]
    return documentsDirectory
}

//MARK: - Create ne folder at path
func createFolderAtThisPath(yourPath:String,folderName:String){
    let dataPath = yourPath.stringByAppendingString("/\(folderName)")
    do {
        try NSFileManager.defaultManager().createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription);
    }
}

func readTextFileFromDocumentDirectory(fileName:String) ->  String{
    let documentUrl = getDocumentsURL()
    var userFacebookid = ""
    if let facebookData = NSUserDefaults.standardUserDefaults().valueForKey(userDefaults.userFacebookInfo.rawValue) as? [String:AnyObject] {
        if let facebookId = facebookData["userFacebookid"] as? String {
            userFacebookid = facebookId
        }
    }
    var dataPath = "\(documentUrl)".stringByAppendingString("/\(userFacebookid)")
    dataPath = dataPath.stringByAppendingString("/"+fileName)
    print(dataPath)
    do {
        let textFileString = try String(contentsOfFile: dataPath)
        return textFileString
    }
    catch _ as NSError {
        return "(\n)"
    }
}

//MARK: Remove saved data on logout
func removePersistedDataOnLogout() {
    let appDomain = NSBundle.mainBundle().bundleIdentifier!
    NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
}

// MARK: - Extension of UIButton to set its background color according to its state
extension UIButton {
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setBackgroundColor(color: UIColor, forUIControlState state: UIControlState) {
        self.setBackgroundImage(imageWithColor(color), forState: state)
    }
}
extension UIView {
    func screenShot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.mainScreen().scale)
        self.drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

