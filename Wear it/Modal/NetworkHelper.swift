//
//  NetworkHelper.swift
//  Wear it
//
//  Created by Rohit Arora on 15/12/15.
//  Copyright © 2015 Rohit Arora. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
public class NetworkHelper {
    class func facebookLoginOperation(controller : UIViewController, completion: (status: Int) -> Void) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["email"], fromViewController: controller, handler: { (result, error) -> Void in
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result
                if(fbloginresult.grantedPermissions != nil) {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((FBSDKAccessToken.currentAccessToken()) != nil){
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                                if (error == nil){
                                    if let extractedResult = result as? NSDictionary {
                                        print(extractedResult)
                                        var userEmailId="",userName = "", userImageUrl="", userFacebookid = ""
                                        if let extractedFbId = extractedResult["id"]! as? NSString {
                                            userFacebookid = extractedFbId as String
                                        }
                                        if let extractedEmailId = extractedResult["email"] as? NSString {
                                            userEmailId = extractedEmailId as String
                                        }
                                        if let picDictionary = extractedResult["picture"]! as? NSDictionary {
                                            if let picData = picDictionary["data"]! as? NSDictionary {
                                                if let imageUrl = picData["url"]! as? String {
                                                    userImageUrl = imageUrl as String
                                                }
                                            }
                                        }
                                        if let extractedFullName = extractedResult["name"] as? NSString {
                                            userName = extractedFullName as String
                                        }
                                        createOrFetchUserInfoDatabaseInDirectory(userFacebookid)
                                        let facebookData = ["userName":userName,"userEmailId":userEmailId,"userImageUrl":userImageUrl,"userFacebookid":userFacebookid]
                                        NSUserDefaults.standardUserDefaults().setObject(facebookData, forKey:userDefaults.userFacebookInfo.rawValue)
                                        completion(status: 1)
                                    }
                                }
                            })
                        }
                        fbLoginManager.logOut()
                    }
                } else {
                    Alert(controller, alertMessage:"Facebook login operation cancelled.")
                    completion(status: 0)
                }
            }
        })
    }
}