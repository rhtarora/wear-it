//
//  Constants.swift
//

import Foundation
import UIKit


func println(object: Any) {
	Swift.print(object)
}
func print(object: Any) {
    Swift.print(object)
}

enum userDefaults : String {
    case userFacebookInfo
}
//Error Message
let ErrorInternetConnection                     = "You may not have internet connection. Please check your connection and try again."
let ConnectionError                             = "Could not connect to the server. Please try again later."
let facebookButtonNormalColor          =     UIColor(red: 53.0/255, green: 82.0/255, blue: 143.0/255, alpha: 1.0)
let facebookButtonPressedColor          =     UIColor(red: 32.0/255, green: 50.0/255, blue: 89.0/255, alpha: 1.0)
let appMainColor                       = UIColor(red: 185.0/255, green: 43.0/255, blue: 39.0/255, alpha: 1.0)

